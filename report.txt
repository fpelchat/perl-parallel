select sp1.groupby, count(sp1.id) * 4
from subset1_perso sp1
where sp1.samplecount <= 25 and sp1.lat is not null
group by sp1.groupby
order by sp1.groupby desc

INTO OUTFILE '/home/phantom/report_subset1_commerciale.csv'
FIELDS TERMINATED BY ';'
ENCLOSED BY '"'
LINES TERMINATED BY '\n'

100 / (select count(*) from subset1_perso sp2 where sp2.groupby = sp1.groupby and sp2.samplecount <= 25)

