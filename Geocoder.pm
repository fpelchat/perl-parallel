package Geocoder;

use strict;
use warnings;

use Exporter;

use vars qw($VERSION @ISA @EXPORT @EXPORT_OK %EXPORT_TAGS);

$VERSION     = 1.00;
@ISA         = qw(Exporter);

@EXPORT      = ();
@EXPORT_OK   = qw(geocode parse update_coord set_attempted_flag);
%EXPORT_TAGS = (DEFAULT => [qw(&geocode &parse &update_coord &set_attempted_flag)]);

use LWP::UserAgent;
use HTTP::Cookies; #cookie jar
use HTTP::Request;
use HTTP::Response;
use DBI;
use Time::HiRes qw(gettimeofday); #uniqueness
use JSON; #automatically use JSON::XS when available
use Data::Dumper;
use URI::Escape;
use Term::ANSIColor qw(:constants);

use Momentum::Common;

sub myprint {
	my $ts = localtime(time);
	printf("[%s] %s\n", $ts, $_[0]);
}

sub mydie {
	my $ts = localtime(time);
	die("[%s] %s\n", $ts, $_[0]);
}

sub geocode {
	my $base_url = 'http://localhost:3000/geocode';
	my $wait_nsecs_before_next_attempt = 2;
	my $max_attempts = 0; #0=infinite

	&mydie("usage: spider(dbh,ua,refaddr)\n") unless @_ == 3;

	my $dbh = $_[0];
	my $ua = $_[1];
	my $refaddr = $_[2];

	my $guid = undef;

	$refaddr = uri_escape_utf8($refaddr);

	my $uri = sprintf('%s/%s', $base_url, $refaddr);

	&myprint("geocoding [$refaddr]");

	my $request = new HTTP::Request('GET', $uri);
	my $response = undef;
	my $content = undef;
	my $count_attempts = 0;

	do  { #retry until it works or until the maximum number of attempts is reached
		&Momentum::Common::ua_randomize($ua);
		$response = $ua->request($request);

		#needed for some reason or else its not gonna match anything
		$content = $response->content;

		if(!$response->is_success()) {
			&myprint("couldn't (" . RED . $response->status_line . RESET . ") geocode [" . YELLOW . $refaddr . RESET . "]; retrying in $wait_nsecs_before_next_attempt seconds...");
		}

		printf("%d\n", $response->code);

		if($response->code != 200 && $response->code != 404) {
			sleep($wait_nsecs_before_next_attempt);
			++$count_attempts;
		} #else 404 not found go to the next address

	} while(($count_attempts < $max_attempts || !$max_attempts) && !$response->is_success() && $response->code != 404);

	return ($response->is_success? $content: undef);
}

sub parse {
	&mydie("usage: extract(dbh,content)\n") unless @_ == 2;

	my ($dbh, $content) = @_;

	my $json = decode_json($content);

	my $components = 0;

	my ($lat, $lng);

	if($json->{'status'} eq 'OK') {
		my @results = \@{$json->{'results'}};
		if(@results == 1) {
			for(my $i = 0; defined($results[0][0]{'address_components'}[$i]); ++$i) {
				for(my $j = 0; defined($results[0][0]{'address_components'}[$i]{'types'}[$j]); ++$j) {
					my $type = $results[0][0]{'address_components'}[$i]{'types'}[$j];
					if($type eq 'street_number' || $type eq 'route' || $type eq 'postal_code' || $type eq 'country' || $type eq 'locality') {
						++$components;
					}
				}

			}

			if($components >= 5) { #perfect match
				$lat = $results[0][0]{'geometry'}{'location'}{'lat'};
				$lng = $results[0][0]{'geometry'}{'location'}{'lng'};
				printf("perfect match latitude=%s, longitude=%s\n", $lat, $lng);
				return ($lat, $lng);
			}
		}
	}

	return (0, 0);
}

sub update_coord {
	die("usage: update(dbh,table,id,latitude,longitude)\n") unless @_ == 5;
	my ($dbh, $tn, $id, $lat, $lng) = @_;
	$dbh->do("UPDATE $tn SET lat=$lat, lng=$lng WHERE id=$id");
	&Geocoder::set_attempted_flag($dbh, $tn, $id);
}

sub set_attempted_flag {
	die("usage: update(dbh,table,id)\n") unless @_ == 3;
	my ($dbh, $tn, $id) = @_;
	$dbh->do("UPDATE $tn SET attempted=1 WHERE id=$id");
}

1;

