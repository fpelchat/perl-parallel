package Momentum::Common;

use strict;
use warnings;

use Exporter;

use vars qw($VERSION @ISA @EXPORT @EXPORT_OK %EXPORT_TAGS $DEFAULT_COOKIE_JAR %DEFAULT_DATABASE_SETTINGS $DEFAULT_IP $DEFAULT_UA);

$VERSION     = 1.00;
@ISA         = qw(Exporter);
@EXPORT      = ();
@EXPORT_OK   = qw(ua_new db_new ua_randomize test_random_ip test_random_ua test_random_bind test_all);
%EXPORT_TAGS = (DEFAULT => [qw(&ua_new &db_new &ua_randomize &test_random_ip &test_random_ua &test_random_bind &test_all)]);

%DEFAULT_DATABASE_SETTINGS = (user => 'root',
	pass => 'toor',
	host => 'localhost',
	port => '3306',
	name => 'refaddr'
);

$DEFAULT_COOKIE_JAR  = q(cookie_jar.lwp);

$DEFAULT_IP = q(ip.txt);

$DEFAULT_UA = q(ua.txt);

use LWP::UserAgent;
use HTTP::Cookies; #cookie jar
use HTTP::Request;
use HTTP::Response;
use HTML::Entities; #decode html entities
use Cwd qw(getcwd); #needed for saving pictures
use DBI; #generic database access
use Time::HiRes qw(gettimeofday); #unique file names

sub microtime {
	my $asFloat = 0;
	if(@_) {
		$asFloat = shift;
	}
	(my $epochseconds, my $microseconds) = gettimeofday;
	my $mt = undef;
	if($asFloat) {
		while(length("$microseconds") < 6) {
			$microseconds = "0$microseconds";
		}
		$mt = "$epochseconds.$microseconds";
	} else {
		#modded to generate unique filename w/o a whitespace
		$mt = "$epochseconds".'_'."$microseconds";
	}
	return $mt;
}

#more randomness without it
#srand;

sub random_ua {
	my $rua = undef;
	open FILE, "<$DEFAULT_UA" or die "could not open ua.txt: $!\n";
	rand($.)<1 and ($rua = $_) while <FILE>;
	close FILE;
	chomp($rua);
#	print "random ua: $rua\n";
	return $rua;
}

sub random_ip {
	my $rip = undef;
	open FILE, "<$DEFAULT_IP" or die "could not open ip.txt: $!\n";
	rand($.)<1 and ($rip = $_) while <FILE>;
	close FILE;
	chomp($rip);
#	print "random ip: $rip\n";
	return $rip;
}

#fetch sources that has at least one data type
sub fetch_sources {
	my $dbh = $_[0];

	my $sql = "
SELECT DISTINCT s.name
FROM sources s 
JOIN spider_data_types t ON t.source_id=s.id 
ORDER BY s.name ASC 
	";

	my $sth = $dbh->prepare($sql) or die "prepare: " . $dbh->errstr();
	$sth->execute() or die "execute: " . $sth->errstr();

	my (@sources, @row);

	while(@row = $sth->fetchrow_array()) {
		my ($name) = @row;
		push(@sources, $name);
	}

	return @sources;
}

sub fetch_data_types_by_source_name {
	my $dbh = $_[0];
	my $qtSourceName = $dbh->quote($_[1]);

	my $sql = "
SELECT DISTINCT t.name
FROM spider_data_types t 
JOIN sources s ON t.source_id=s.id 
WHERE s.name = $qtSourceName
ORDER BY t.name ASC 
	";

	my $sth = $dbh->prepare($sql) or die "prepare: " . $dbh->errstr();
	$sth->execute() or die "execute: " . $sth->errstr();

	my (@data_types, @row);

	while(@row = $sth->fetchrow_array()) {
		my ($name) = @row;
		push(@data_types, $name);
	}

	return @data_types;
}

sub fetch_tickers_list_methods {
	my $dbh = $_[0];

	my $sql = "
SELECT m.name
FROM tickers_list_methods m 
ORDER BY m.name ASC 
	";

	my $sth = $dbh->prepare($sql) or die "prepare: " . $dbh->errstr();
	$sth->execute() or die "execute: " . $sth->errstr();

	my (@methods, @row);

	while(@row = $sth->fetchrow_array()) {
		my ($name) = @row;
		push(@methods, $name);
	}

	return @methods;
}

sub fetch_subroutine_by_tickers_list_method {
	my $dbh = $_[0];
	my $method_system_name = $_[1];
	my $qt_method = $dbh->quote($method_system_name);

	my $sql = "
SELECT m.perl_subroutine
FROM tickers_list_methods m 
WHERE m.system_name=$qt_method
LIMIT 1 
	";

	my $sth = $dbh->prepare($sql) or die "prepare: " . $dbh->errstr();
	$sth->execute() or die "execute: " . $sth->errstr();

	my @row = ();
	my $sub = undef;

	while(@row = $sth->fetchrow_array()) {
		($sub) = @row;
	}

	if(!defined($sub)) {
		die("Cannot find subroutine for $method_system_name\n");
	}

	return $sub;
}

sub fetch_package_by_source_data_type {
	my $dbh = $_[0];
	my $source_system_name = $_[1];
	my $data_type_system_name = $_[2];
	my $qt_source = $dbh->quote($source_system_name);
	my $qt_data_type = $dbh->quote($data_type_system_name);

	my $sql = "
SELECT t.perl_package 
FROM spider_data_types t 
JOIN sources s ON s.id=t.source_id 
WHERE s.system_name=$qt_source AND t.system_name=$qt_data_type 
LIMIT 1 
	";

	my $sth = $dbh->prepare($sql) or die "prepare: " . $dbh->errstr();
	$sth->execute() or die "execute: " . $sth->errstr();

	my @row = ();
	my $package = undef;

	while(@row = $sth->fetchrow_array()) {
		($package) = @row;
	}

	if(!defined($package)) {
		die("Cannot find package for $source_system_name + $data_type_system_name\n");
	}

	return $package;
}

sub db_new {
	my $conn_str = sprintf("DBI:mysql:%s;host=%s;port=%s",
		$DEFAULT_DATABASE_SETTINGS{'name'},
		$DEFAULT_DATABASE_SETTINGS{'host'},
		$DEFAULT_DATABASE_SETTINGS{'port'}
	);

	my $dbh = DBI->connect($conn_str,
		$DEFAULT_DATABASE_SETTINGS{'user'},
		$DEFAULT_DATABASE_SETTINGS{'pass'},
		{RaiseError => 1, AutoCommit => 1}
	);

	return $dbh;
}

sub ua_new {
	my $ua = new LWP::UserAgent;
	my $cookie_jar = HTTP::Cookies->new('file' => $DEFAULT_COOKIE_JAR, 'autosave' => 1);

	$ua->cookie_jar($cookie_jar);

	return $ua;
}

sub ua_randomize {
	@LWP::Protocol::http::EXTRA_SOCK_OPTS = (LocalAddr => &random_ip());

	$_[0]->agent(&random_ua()); #everything is passed by reference in perl
}

sub test_random_ua {
	print "===============================================\n";
	print "testing random ua generation\n";
	print "===============================================\n";
	for(my $i = 0; $i < 20; $i++) {
		&random_ua();
	}
}

sub test_random_ip {
	print "===============================================\n";
	print "testing random ip generation\n";
	print "===============================================\n";
	for(my $i = 0; $i < 20; $i++) {
		&random_ip();
	}
}

sub test_random_bind {
	print "===============================================\n";
	print "testing random bind()\n";
	print "===============================================\n";
	my $ua = &ua_new();

	for(my $i = 0; $i < 20; $i++) {
		&ua_randomize($ua);

		my $action = new HTTP::Request('GET', 'http://showmyipaddress.com/');
		my $html = $ua->request($action);

		if($html->content =~ m/<big><strong>([^<]+)</) {
			print "http://showmyipaddress.com/ sees you as: $1\n";
		} else {
			print "was not able to extract the ip address\n";
		}
	}
}

sub test_all {
	&test_random_ip();
	&test_random_ua();
	&test_random_bind();
}

1;
