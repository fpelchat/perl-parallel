#!/bin/bash
if [ $# -ne 1 ]; then
	echo "$0 <subset table>"
	exit
fi
function mysql_query {
  mysql --local-infile -u root --password=toor -e "$1" refaddr
}
table=$1
mysql_query "DROP TABLE IF EXISTS ${table}"
mysql_query "CREATE TABLE ${table} (id int auto_increment primary key,groupby char(6),updated_at date,street_number varchar(10),street_name varchar(50),postal_code varchar(10),city varchar(50),country varchar(50),samplecount int, lat double, lng double, attempted int(1));"
mysql_query "LOAD DATA LOCAL INFILE './${table}.csv' IGNORE INTO TABLE refaddr.${table} FIELDS TERMINATED BY ';' ENCLOSED BY '\"' LINES TERMINATED BY '\\r\\n' (id,groupby,updated_at,street_number,street_name,city,postal_code,country,samplecount)"
