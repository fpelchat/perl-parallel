package Addr;

use strict;
use warnings;

use Exporter;

use vars qw($VERSION @ISA @EXPORT @EXPORT_OK %EXPORT_TAGS);

$VERSION     = 1.00;
@ISA         = qw(Exporter);

@EXPORT      = ();
@EXPORT_OK   = qw(
		fetch_refaddr
	);
%EXPORT_TAGS = (DEFAULT => [qw(
		&fetch_refaddr
	)]);

use DBI;

sub fetch_refaddr {
	my $dbh = $_[0];
	my $tn = $_[1];
	my $maxpermonth = $_[2];

	my $sql = "
SELECT id,
	updated_at,
	street_number,
	street_name,
	postal_code,
	city,
	country
FROM $tn
WHERE samplecount <= $maxpermonth AND
	(attempted = 0 OR attempted IS NULL)
ORDER BY groupby DESC
	";

	my $sth = $dbh->prepare($sql) or die "prepare: " . $dbh->errstr();
	$sth->execute() or die "execute: " . $sth->errstr();

	return $sth;
}

1;

